using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vaina2 : MonoBehaviour
{
    [SerializeField]
    float speed = 5;
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
   
            if (Input.GetKey(KeyCode.RightArrow))
            {
                other.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
 
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                other.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
            }
            else
            {
                other.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }
        }
}
}

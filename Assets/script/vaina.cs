using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vaina : MonoBehaviour
{
    [SerializeField]
    float speed = 5;
    public Animator animator;
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
   
            if (Input.GetKey(KeyCode.UpArrow) || (Input.GetKey(KeyCode.W)))
            {
                other.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
                animator.SetBool("Climb", true);
            }
            else if (Input.GetKey(KeyCode.DownArrow) || (Input.GetKey(KeyCode.S)))
            {
                other.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
                animator.SetBool("Climb", true);
            }
            else
            {
                other.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                animator.SetBool("Climb", false);
            }
            
        }
}

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            animator.SetBool("Climb", false);
        }
}
}

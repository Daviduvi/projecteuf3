using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemyDamage : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D col) {
        if (col.transform.CompareTag("Player")) {
            Destroy(col.gameObject);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}

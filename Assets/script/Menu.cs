using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    public GameObject menu;


    public void Update() {
        
        if(Input.GetKeyDown(KeyCode.Escape)) {
            OpenMenu();
            
        }

    }

    public void OpenMenu() {
        Time.timeScale = 0;
        menu.SetActive(true);
    }

    public void MainMenu() {
        SceneManager.LoadScene("Menu");
    }

    public void ResumeGame() {
        Time.timeScale = 1;
        menu.SetActive(false);
    }

    public void QuitGame() {
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void EscenaJuego() {
        SceneManager.LoadScene("pt1");
        Time.timeScale = 1;
    }

    public void Exit() {
        Application.Quit();
    }
}
